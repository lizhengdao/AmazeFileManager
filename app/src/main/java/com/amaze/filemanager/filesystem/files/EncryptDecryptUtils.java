/*
 * Copyright (C) 2014-2020 Arpit Khurana <arpitkh96@gmail.com>, Vishal Nehra <vishalmeham2@gmail.com>,
 * Emmanuel Messulam<emmanuelbendavid@gmail.com>, Raymond Lai <airwave209gt at gmail.com> and Contributors.
 *
 * This file is part of Amaze File Manager.
 *
 * Amaze File Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.amaze.filemanager.filesystem.files;

import java.io.IOException;
import java.security.GeneralSecurityException;

import com.amaze.filemanager.R;
import com.amaze.filemanager.asynchronous.management.ServiceWatcherUtil;
import com.amaze.filemanager.asynchronous.services.DecryptService;
import com.amaze.filemanager.asynchronous.services.EncryptService;
import com.amaze.filemanager.database.CryptHandler;
import com.amaze.filemanager.database.models.explorer.EncryptedEntry;
import com.amaze.filemanager.filesystem.HybridFileParcelable;
import com.amaze.filemanager.ui.activities.MainActivity;
import com.amaze.filemanager.ui.dialogs.GeneralDialogCreation;
import com.amaze.filemanager.ui.fragments.MainFragment;
import com.amaze.filemanager.ui.fragments.preference_fragments.PreferencesConstants;
import com.amaze.filemanager.ui.provider.UtilitiesProvider;
import com.amaze.filemanager.utils.OpenMode;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.widget.Toast;

/**
 * Provides useful interfaces and methods for encryption/decryption
 *
 * @author Emmanuel on 25/5/2017, at 16:55.
 */
public class EncryptDecryptUtils {

  public static final String DECRYPT_BROADCAST = "decrypt_broadcast";
  /**
   * Queries database to map path and password. Starts the encryption process after database query
   *
   * @param path the path of file to encrypt
   * @param password the password in plaintext
   * @throws GeneralSecurityException Errors on encrypting file/folder
   * @throws IOException I/O errors on encrypting file/folder
   */
  public static void startEncryption(
      Context c, final String path, final String password, Intent intent)
      throws GeneralSecurityException, IOException {
    CryptHandler cryptHandler = CryptHandler.getInstance();
    String destPath =
        path.substring(0, path.lastIndexOf('/') + 1)
            .concat(intent.getStringExtra(EncryptService.TAG_ENCRYPT_TARGET));

    // EncryptService.TAG_ENCRYPT_TARGET already has the .aze extension, no need to append again

    EncryptedEntry encryptedEntry = new EncryptedEntry(destPath, password);
    cryptHandler.addEntry(encryptedEntry);

    // start the encryption process
    ServiceWatcherUtil.runService(c, intent);
  }



  public interface EncryptButtonCallbackInterface {

    /** Callback fired when we've just gone through warning dialog before encryption */
    void onButtonPressed(Intent intent) throws GeneralSecurityException, IOException;

    /**
     * Callback fired when user has entered a password for encryption Not called when we've a master
     * password set or enable fingerprint authentication
     *
     * @param password the password entered by user
     */
    void onButtonPressed(Intent intent, String password)
        throws GeneralSecurityException, IOException;
  }

  public interface DecryptButtonCallbackInterface {
    /** Callback fired when we've confirmed the password matches the database */
    void confirm(Intent intent);

    /** Callback fired when password doesn't match the value entered by user */
    void failed();
  }
}
